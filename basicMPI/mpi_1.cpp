//=============================================================================
// Simple MPI program example 1
// It just print the rank of each process
//
// This is a part of teaching material provided for the following workshop
// "MPI in action: Parallelization of unsteady heat conduction solvers"
// conducted by IIT Goa - Nodal centre of National Supercomputing Mission
// Date: 2--3 October 2021
//
// Written by Y Sudhakar, IIT Goa
// email: sudhakar@iitgoa.ac.in
//=============================================================================
#include<mpi.h>
#include<iostream>

int main(int argc, char**argv)
{
	int rank, size;
	MPI_Init(&argc, &argv);								 // initialize
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);  // get rank of each process
	MPI_Comm_size(MPI_COMM_WORLD, &size);  // get no of process in this communicator

	std::cout<<"I am " <<rank<<" of "<< size <<" process"<<std::endl;
	MPI_Finalize();
}
