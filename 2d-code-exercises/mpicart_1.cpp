//=============================================================================
// A simple code to illustrate concepts of MPI_Cart
//
// This is a part of teaching material provided for the following workshop
// "MPI in action: Parallelization of unsteady heat conduction solvers"
// conducted by IIT Goa - Nodal centre of National Supercomputing Mission
// Date: 2--3 October 2021
//
// Written by Y Sudhakar, IIT Goa
// email: sudhakar@iitgoa.ac.in
//
// TODO list for the participants
// 1. Fix the bug
// 2. check the output: what does -ve rank imply?
// 3. activate periodic in x-direction. How and why results change?
//=============================================================================
#include <iostream>
#include <mpi.h>
#include <cmath>
#include <fstream>


// ---
// main cpp file
// ---
int main(int argc, char **argv)
{
  // size of the global mesh (exclusing ghost nodes). This will be shared across processors
  int nxg = 8, nyg = 8;

  // no of processors across which x- and y- part of mesh will be distributed
  int npx = 2, npy = 2;

  // Initialize MPI
  MPI_Init(&argc, &argv);
  int procsize; // number of processors
  MPI_Comm_size(MPI_COMM_WORLD, &procsize);

  // Ask MPI to decompose our processors in a 2D cartesian grid for us
  int dims[2] = {npx, npy};
  MPI_Dims_create(procsize, 2, dims);

  // If periodic BC is true the it connects appropriate processors
  // by constructing neighbor information
  int periods[2] = {false, false};

  // Create new communicator for the Cartesian structure from MPI_COMM_WORLD
  // Note that we still include all cores -- but ranks can be different
  // in these communicators
  int reorder = true;
  MPI_Comm new_comm;
  // Create Catesian grid of processes
  MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &new_comm);

  // Get rank of each process in the communicator
  int my_rank;
  MPI_Comm_rank(new_comm, &my_rank);

  // Get my coordinates in the new communicator
  int my_coords[2];
  MPI_Cart_coords(new_comm, my_rank, 2, my_coords);
	printf("[MPI process %d] I am located at (%d, %d).\n", my_rank, my_coords[0], my_coords[1]);

  MPI_Barrier( new_comm );

  // Declare our neighbours
  // The neighbor information is needed to communicate updated values
  // here
  // neighbours_ranks[0] -- left neighbor
  // neighbours_ranks[1] -- right neighbor
  // neighbours_ranks[2] -- bottom neighbor
  // neighbours_ranks[3] -- top neighbor
  int neighbours_ranks[4];

  // Let consider dims[0] = X, so the shift tells us our left and right neighbours
  MPI_Cart_shift(new_comm, 0, 1, &neighbours_ranks[0], &neighbours_ranks[1]);

  // Let consider dims[1] = Y, so the shift tells us our up and down neighbours
  MPI_Cart_shift(new_comm, 1, 1, &neighbours_ranks[2], &neighbours_ranks[3]);

  if( my_rank == 0)
  {
    std::cout<<neighbours_ranks[0]<<" "<<neighbours_ranks[1]<<" "
             <<neighbours_ranks[2]<<" "<<neighbours_ranks[3]<<std::endl;
  }

  return 0;
}
