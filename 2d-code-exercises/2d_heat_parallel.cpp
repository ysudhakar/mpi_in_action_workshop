//=============================================================================
// Solution of unsteady 2D diffusion equation withinin the
// domain [0,1]^2 using finite difference method.
// MPI libraries are used for parallelization.
// We make use of MPI_Cart features.
//
// The problem we solve has the exact solution
// u(x,y) = sin(2*pi*x)sin(2*pi*y)
// Homogeneous Dirichlet BC on top bottom, left and right
//
// This is a part of teaching material provided for the following workshop
// "MPI in action: Parallelization of unsteady heat conduction solvers"
// conducted by IIT Goa - Nodal centre of National Supercomputing Mission
// Date: 2--3 October 2021
//
// Written by Y Sudhakar, IIT Goa
// email: sudhakar@iitgoa.ac.in
//
// TODO list for the participants
// 1. Complete the code
// 2. Make periodic BC for left and right
// change ff[ii][jj] = 8.0*M_PI*M_PI*sin(2*M_PI*(xx[ii]+0.25))*sin(2*M_PI*yy[jj]);
// 3. Write 3D code
//=============================================================================
#include <iostream>
#include <mpi.h>
#include <cmath>
#include <fstream>
#include <iomanip>      // std::setw
#include <string>
#include <sstream>
#include <map>

#include <algorithm>
#include <chrono>

using namespace std::chrono; // to compute time taken

// Function declarations
std::string get_local_filename( int idp );


// ---
// main cpp file
// ---
int main(int argc, char **argv)
{
  // size of the global mesh (exclusing ghost nodes). This will be shared across processors
  int nxg = 32, nyg = 32;

  // no of processors across which x- and y- part of mesh will be distributed
  int npx = 2, npy = 2;

  // Initialize MPI
  MPI_Init(&argc, &argv);
  int procsize; // number of processors
  MPI_Comm_size(MPI_COMM_WORLD, &procsize);

  // Ask MPI to decompose our processors in a 2D cartesian grid for us
  int dims[2] = {npx, npy};
  MPI_Dims_create(procsize, 2, dims);

  // If periodic BC is true the it connects appropriate processors
  // by constructing neighbor information
  int periods[2] = {false, false};

  // Create new communicator for the Cartesian structure from MPI_COMM_WORLD
  // Note that we still include all cores -- but ranks can be different
  // in these communicators
  int reorder = true;
  MPI_Comm new_comm;
  // Create Catesian grid of processes
  MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &new_comm);

  // Get rank of each process in the communicator
  int my_rank;
  MPI_Comm_rank(new_comm, &my_rank);

  // Get my coordinates in the new communicator
  int my_coords[2];
  MPI_Cart_coords(new_comm, my_rank, 2, my_coords);
	//printf("[MPI process %d] I am located at (%d, %d).\n", my_rank, my_coords[0], my_coords[1]);

  // Declare our neighbours
  // The neighbor information is needed to communicate updated values
  // here
  // neighbours_ranks[0] -- left neighbor
  // neighbours_ranks[1] -- right neighbor
  // neighbours_ranks[2] -- bottom neighbor
  // neighbours_ranks[3] -- top neighbor
  int neighbours_ranks[4];

  // Let consider dims[0] = X, so the shift tells us our left and right neighbours
  MPI_Cart_shift(new_comm, 0, 1, &neighbours_ranks[0], &neighbours_ranks[1]);

  // Let consider dims[1] = Y, so the shift tells us our up and down neighbours
  MPI_Cart_shift(new_comm, 1, 1, &neighbours_ranks[2], &neighbours_ranks[3]);

  // get global index ranges for each processor
  int xnum = nxg/npx;
  int ynum = nyg/npy;
  int ibegin = my_coords[0]*xnum;
  int iend = ibegin+xnum+1; // the extra for the ghost nodes in each processor
  int jbegin = my_coords[1]*ynum;
  int jend = jbegin+ynum+1; // the extra for the ghost nodes in each processor

  // no of elements per processor; extra 2 is for the ghost nodes on either sides
  xnum = xnum + 2;
  ynum = ynum + 2;
  double uu[xnum][ynum], uold[xnum][ynum];
  double xx[xnum], yy[ynum];

  // compute location of the nodes within each processor
  double dx = 1.0/(nxg-1);
  double dy = 1.0/(nyg-1);
  for (int ii=0; ii<xnum; ii++)
    xx[ii] = (ii+ibegin-1)*dx;
  for (int jj=0; jj<ynum; jj++)
    yy[jj] = (jj+jbegin-1)*dy;

  // initialize the solution
  for(int ii=0; ii<xnum; ii++)
  {
    for(int jj=0; jj<ynum; jj++)
    {
      uu[ii][jj] = 0.0;
      uold[ii][jj] = 0.0;
    }
  }

  // Since source term is constant we evalute them only once before the timeloop
  double ff[xnum][ynum];
  for(int ii=0; ii<xnum; ii++)
  {
    for(int jj=0; jj<ynum; jj++)
    {
      //ff[ii][jj] = 8.0*M_PI*M_PI*sin(2*M_PI*(xx[ii]+0.25))*sin(2*M_PI*yy[jj]);
      ff[ii][jj] = 8.0*M_PI*M_PI*sin(2*M_PI*xx[ii])*sin(2*M_PI*yy[jj]);
    }
  }

  // start time
  auto start = high_resolution_clock::now();

  // beginning of time loop
  double time_end = 1.0, dt = 0.000001, time = 0.0;
  double tempx=0.0, tempy=0.0, ftemp = 0.0, temp = 0.0;
  double dx2inv = 1.0/dx/dx;
  double dy2inv = 1.0/dy/dy;
  double maxer_loc = 0.0, maxer_glo = 0.0;
  int iter = 0;
	while ( 1 )
	{
    iter++;
		time += dt;

    maxer_loc = 0.0;
    for(int ii=1; ii<xnum-1; ii++)
    {
      for(int jj=1; jj<ynum-1; jj++)
      {
        // the following lines take care of homogeneous Dirichlet BC
        if( jj==1 and neighbours_ranks[2] == MPI_PROC_NULL )
          continue;
        if( jj==ynum-2 and neighbours_ranks[3] == MPI_PROC_NULL )
          continue;
        if( ii == 1 and neighbours_ranks[0] == MPI_PROC_NULL )
          continue;
        if( ii == xnum-2 and neighbours_ranks[1] == MPI_PROC_NULL )
          continue;

        tempx = (uold[ii+1][jj]-2.0*uold[ii][jj]+uold[ii-1][jj])*dx2inv;
        tempy = (uold[ii][jj+1]-2.0*uold[ii][jj]+uold[ii][jj-1])*dy2inv;
        temp = tempx + tempy + ff[ii][jj];
        uu[ii][jj] = uold[ii][jj] + dt*temp;

        if(fabs(temp) > maxer_loc)
          maxer_loc = fabs(temp);
      }
    }

  // Exchange information across processors
  //neighbours_ranks[0] == left
  //neighbours_ranks[1] == right
  //neighbours_ranks[2] == down
  //neighbours_ranks[3] == up

  // === IMPLEMENT MESSAGE PASSING ACROSS PRCESSORS
  // STEP1 :Sending information to the left processor
  if( neighbours_ranks[0] != MPI_PROC_NULL)
  {
    double left[ynum];
    for( int ii=0; ii<ynum; ii++ )
      left[ii] = uu[1][ii];
    MPI_Send(&left, ynum, MPI_DOUBLE, neighbours_ranks[0], my_rank, new_comm);
  }
  // Receiving information from the right processor
  if( neighbours_ranks[1] != MPI_PROC_NULL)
  {
    double right1[ynum];
    MPI_Recv(&right1, ynum, MPI_DOUBLE, neighbours_ranks[1], neighbours_ranks[1], new_comm, MPI_STATUS_IGNORE);
    for( int ii=0; ii<ynum; ii++ )
      uu[xnum-1][ii] = right1[ii];
  }
  MPI_Barrier(new_comm);

  // STEP2 :Sending information to the right processor
  if( neighbours_ranks[1] != MPI_PROC_NULL)
  {
    double right[ynum];
    for( int ii=0; ii<ynum; ii++ )
      right[ii] = uu[xnum-2][ii];
    MPI_Send(&right, ynum, MPI_DOUBLE, neighbours_ranks[1], my_rank, new_comm);
  }
  // Receiving information from the left processor
  if( neighbours_ranks[0] != MPI_PROC_NULL)
  {
     double left1[ynum];
     MPI_Recv(&left1, ynum, MPI_DOUBLE, neighbours_ranks[0], neighbours_ranks[0], new_comm, MPI_STATUS_IGNORE);
     for( int ii=0; ii<ynum; ii++ )
       uu[0][ii] = left1[ii];
  }
  MPI_Barrier(new_comm);

  // STEP3 :Sending information to the down processor
  if( neighbours_ranks[2] != MPI_PROC_NULL)
  {
    double left[xnum];
    for( int ii=0; ii<xnum; ii++ )
      left[ii] = uu[ii][1];
    MPI_Send(&left, xnum, MPI_DOUBLE, neighbours_ranks[2], my_rank, new_comm);
  }
  // Receiving information from the top processor
  if( neighbours_ranks[3] != MPI_PROC_NULL)
  {
    double right1[xnum];
    MPI_Recv(&right1, xnum, MPI_DOUBLE, neighbours_ranks[3], neighbours_ranks[3], new_comm, MPI_STATUS_IGNORE);
    for( int ii=0; ii<xnum; ii++ )
      uu[ii][ynum-1] = right1[ii];
  }
  MPI_Barrier(new_comm);

  // STEP4 :Sending information to the top processor
  if( neighbours_ranks[3] != MPI_PROC_NULL)
  {
    double right[xnum];
    for( int ii=0; ii<xnum; ii++ )
      right[ii] = uu[ii][ynum-2];
    MPI_Send(&right, xnum, MPI_DOUBLE, neighbours_ranks[3], my_rank, new_comm);
  }
  // Receiving information from the bottom processor
  if( neighbours_ranks[2] != MPI_PROC_NULL)
  {

    double left1[xnum];
    MPI_Recv(&left1, xnum, MPI_DOUBLE, neighbours_ranks[2], neighbours_ranks[2], new_comm, MPI_STATUS_IGNORE);
    for( int ii=0; ii<xnum; ii++ )
      uu[ii][0] = left1[ii];
  }
  MPI_Barrier(new_comm);


  // copy new values to old values
  for(int ii=0; ii<xnum; ii++)
  {
    for(int jj=0; jj<ynum; jj++)
      uold[ii][jj] = uu[ii][jj];
  }

  // compute max value across all processors
  MPI_Allreduce(&maxer_loc, &maxer_glo, 1, MPI_DOUBLE, MPI_MAX,
             new_comm);

  // print output of the maximum error
  if( my_rank == 0 and iter%1000 == 0 )
    std::cout << iter <<" "<<maxer_glo << std::endl;

  // if error is less than a predefined value, we break the loop
  if( maxer_glo < 1e-8 )
    break;
	}

  // end time
  auto stop = high_resolution_clock::now();
  // runtime
  auto duration = duration_cast<microseconds>(stop - start);
  if( my_rank ==0 )
    std::cout << "Time taken : " << duration.count() << " microseconds" << std::endl;

  // print the final error
  if( my_rank ==0 )
    std::cout << iter <<" "<<maxer_glo << std::endl;


    // ========= The following are for printing the output =============== //
    // ========= For now, take this for granted ========================== //
    // Get the map of <proc id><ibegin,iend,jbegin,jend> from all proc to the root
    // This will be used in the global output file
    int ijproc[4] = {ibegin, iend, jbegin, jend};
    std::map<int, int> pibegin, piend, pjbegin, pjend;

    // store values of the root processor
    pibegin.insert(std::pair<int, int>(0, ijproc[0]));
    piend.insert(std::pair<int, int>(0, ijproc[1]));
    pjbegin.insert(std::pair<int, int>(0, ijproc[2]));
    pjend.insert(std::pair<int, int>(0, ijproc[3]));

    // store values from other processors
    // (This probably is an inefficient way; there should be a better way)
    // (We should avoid MPI_Barrier)
    for( int ip = 1; ip < procsize; ip++ )
    {
        if( my_rank == ip )
            MPI_Send(&ijproc, 4, MPI_DOUBLE, 0, 0, new_comm);
        if( my_rank == 0 )
        {
            MPI_Recv(&ijproc, 4, MPI_DOUBLE, ip, 0, new_comm, MPI_STATUS_IGNORE);
            pibegin.insert(std::pair<int, int>(ip, ijproc[0]));
            piend.insert(std::pair<int, int>(ip, ijproc[1]));
            pjbegin.insert(std::pair<int, int>(ip, ijproc[2]));
            pjend.insert(std::pair<int, int>(ip, ijproc[3]));
        }
        MPI_Barrier(new_comm);
    }


    // write global output file
    if( my_rank == 0 )
    {
        std::ofstream outf;
        outf.open("output.pvtr");
        outf << "<VTKFile type=\"PRectilinearGrid\">" << std::endl;
        outf << "<PRectilinearGrid WholeExtent=\"" << 0 <<" " << nxg+1 <<" 0 " << nyg+1 <<" 0 0\" GhostLevel=\"1\">" << std:: endl;
        outf << "<PPointData>" <<std::endl;
        outf << "\t <PDataArray type=\"Float32\" Name=\"scalar\" NumberOfComponents=\"1\"/>" << std::endl;
        outf << "\t <PDataArray type=\"Float32\" Name=\"proc id\" NumberOfComponents=\"1\"/>" << std::endl;
        outf << "</PPointData>" << std::endl;
        outf << "<PCellData></PCellData>" << std::endl;
        outf << "<PCoordinates>" << std::endl;
        outf << "\t <PDataArray type=\"Float32\" Name=\"X_COORDINATES\" NumberOfComponents=\"1\"/>" << std::endl;
        outf << "\t <PDataArray type=\"Float32\" Name=\"Y_COORDINATES\" NumberOfComponents=\"1\"/>" << std::endl;
        outf << "\t <PDataArray type=\"Float32\" Name=\"Z_COORDINATES\" NumberOfComponents=\"1\"/>" << std::endl;
        outf << "</PCoordinates>" << std::endl;
        for (int ii=0; ii< procsize; ii++ )
        {
            // TODO: While running in parallel, ibegin, iend, jbegin, jend are not available for all processors. We are in the loop my_rank==0
            std::string file_local = get_local_filename( ii );
            outf << "<Piece Extent= \"" << pibegin[ii] << " " << piend[ii] <<" " << pjbegin[ii] << " " << pjend[ii] <<" 0 0\" Source=\""<< file_local <<"\"/>" << std::endl;
        }
        outf << "</PRectilinearGrid>" << std::endl;
        outf << "</VTKFile>" << std::endl;
        outf.close();
    }

    // write file of individual files
    std::string file_local = get_local_filename( my_rank );
    std::ofstream outl;
    outl.open(file_local);
    outl << "<VTKFile type=\"RectilinearGrid\" version=\"0.1\">" << std::endl;
    outl << "<RectilinearGrid WholeExtent=\"" << 0 <<" " << nxg+1 <<" 0 " << nyg+1 <<" 0 0\" GhostLevel=\"1\">" << std:: endl;
    outl << "<Piece Extent= \"" << ibegin << " " << iend <<" " << jbegin << " " << jend << " 0 0\">" << std::endl;
    outl << "<PointData>" << std::endl;
    outl << "<DataArray type=\"Float32\" Name=\"scalar\" NumberOfComponents=\"1\" format=\"ascii\">" << std::endl;
    // TODO: Is changing order is the only solution?
    // write solution output
    for (int jj=0; jj<ynum; jj++ )
    {
        for (int ii=0; ii< xnum; ii++ )
            outl << uu[ii][jj] << std::endl;
    }
    outl << "</DataArray>" << std::endl;

    // Write processor id as the output
    outl << "<DataArray type=\"Float32\" Name=\"proc id\" NumberOfComponents=\"1\" format=\"ascii\">" << std::endl;
    for (int jj=0; jj<ynum; jj++ )
    {
        for (int ii=0; ii< xnum; ii++ )
            outl << my_rank << std::endl;
    }
    outl << "</DataArray>" << std::endl;

    outl << "</PointData>" << std::endl;
    outl << "<CellData></CellData>" << std::endl;
    outl << "<Coordinates>" << std::endl;
    // write x-coordinates
    outl << "<DataArray type=\"Float32\" Name=\"X_COORDINATES\" NumberOfComponents=\"1\" format=\"ascii\">" <<std::endl;
    for (int ii=0; ii< xnum; ii++ )
        outl << xx[ii] <<std::endl;
    outl << "</DataArray>" << std::endl;

    // write y-coordinates
    outl << "<DataArray type=\"Float32\" Name=\"Y_COORDINATES\" NumberOfComponents=\"1\" format=\"ascii\">" <<std::endl;
    for (int jj=0; jj<ynum; jj++ )
        outl << yy[jj] <<std::endl;
    outl << "</DataArray>" << std::endl;

    // write z-coordinates
    outl << "<DataArray type=\"Float32\" Name=\"Z_COORDINATES\" NumberOfComponents=\"1\" format=\"ascii\">" <<std::endl;
    outl << "0" <<std::endl;
    outl << "</DataArray>" << std::endl;
    outl << "</Coordinates>" << std::endl;
    outl << "</Piece>" << std::endl;
    outl << "</RectilinearGrid>" << std::endl;
    outl << "</VTKFile>" << std::endl;
    outl.close();

	MPI_Finalize();
	return 0;
}

// ---
// Get filename for individual vtr processor
// ---
std::string get_local_filename( int idp )
{
  // individual processor filename
  // we convert myrank into a 4 digit number so that 10 will be counted after 9 while reading in paraview
  std::stringstream sstm;
  sstm << "out_" << std::setfill('0') << std::setw(4) << idp << ".vrt";
  std::string file_local = sstm.str();

  return file_local;
}
