//=============================================================================
// Solution of steady 2D diffusion equation withinin the
// domain [0,1]^2 using finite difference method.
//
// The problem we solve has the exact solution
// u(x,y) = sin(2*pi*x)sin(2*pi*y)
// Homogeneous Dirichlet BC on top and bottom
// periodic condition on left and right
//
// This is a part of teaching material provided for the following workshop
// "MPI in action: Parallelization of unsteady heat conduction solvers"
// conducted by IIT Goa - Nodal centre of National Supercomputing Mission
// Date: 2--3 October 2021
//
// Written by Y Sudhakar, IIT Goa
// email: sudhakar@iitgoa.ac.in
//
// TODO list for the participants
// 1. Debug this code
// 2. Parallelize it using MPI
//=============================================================================
#include <iostream>
#include <cmath>
#include <fstream>

// ---
// main cpp file
// ---
int main(int argc, char **argv)
{
  // number of nodes in each direction
  int xnum = 32, ynum = 32;

  double uu[xnum][ynum], uold[xnum][ynum], ff[xnum][ynum];
  double xx[xnum], yy[ynum];

  // coefficients of the matrix
  double ap[xnum][ynum];
  double aw[xnum][ynum], ae[xnum][ynum];
  double as[xnum][ynum], an[xnum][ynum];

  // compute location of the nodes within each processor
  double dx = 1.0/(xnum-1);
  double dy = 1.0/(ynum-1);
  for (int ii=0; ii<xnum; ii++)
    xx[ii] = ii*dx;
  for (int jj=0; jj<ynum; jj++)
    yy[jj] = jj*dy;

  // initialize the solution
  for(int ii=0; ii<xnum; ii++)
  {
    for(int jj=0; jj<ynum; jj++)
    {
      uu[ii][jj] = 0.0;
      uold[ii][jj] = 0.0;
      // Since source term is constant we evalute them only once before the timeloop
      ff[ii][jj] = dx*dx*8.0*M_PI*M_PI*sin(2*M_PI*xx[ii])*sin(2*M_PI*yy[jj]);

      // initialization of coefficients
      ap[ii][jj] = 1.0;
      ae[ii][jj] = 0.0;
      aw[ii][jj] = 0.0;
      as[ii][jj] = 0.0;
      an[ii][jj] = 0.0;
    }
  }


  // actual coefficients
  double dx2inv = 1.0/dx/dx;
  double dy2inv = 1.0/dy/dy;
  for(int ii=1; ii<xnum-1; ii++)
  {
    for(int jj=1; jj<ynum-1; jj++)
    {
      // initialization of coefficients
      ap[ii][jj] = 4;
      if( ii == 1 )
        aw[ii][jj] = 0.0;
      else
        aw[ii][jj] = -1.0;
      if( ii == xnum-2 )
        ae[ii][jj] = 0.0;
      else
        ae[ii][jj] = -1.0;
      if( jj == 1 )
        as[ii][jj] = 0.0;
      else
        as[ii][jj] = -1.0;
      if( jj == ynum-2 )
        an[ii][jj] = 0.0;
      else
        an[ii][jj] = -1.0;
    }
  }

  // beginning of Jacobi iterations
  double maxer = 0.0, tempx = 0.0, tempy = 0.0;
  int iter = 0;
	while ( 1 )
	{
    iter++;

    maxer = 0.0;
    for(int ii=1; ii<xnum-1; ii++)
    {
      for(int jj=1; jj<ynum-1; jj++)
      {
        tempx = -aw[ii][jj] * uold[ii-1][jj]-ae[ii][jj] * uold[ii+1][jj];
        tempy = -as[ii][jj] * uold[ii][jj-1]-an[ii][jj] * uold[ii][jj+1];
        uu[ii][jj] = (tempx + tempy + ff[ii][jj] * dx2inv)/ap[ii][jj];

        if(fabs(uu[ii][jj] - uold[ii][jj]) > maxer)
          maxer = fabs(uu[ii][jj] - uold[ii][jj]);
      }
    }

  // copy new values to old values
  for(int ii=0; ii<xnum; ii++)
  {
    for(int jj=0; jj<ynum; jj++)
      uold[ii][jj] = uu[ii][jj];
  }

  // print output of the maximum error
  if( iter%1000 == 0 )
    std::cout << iter <<" "<<maxer << std::endl;

  // if error is less than a predefined value, we break the loop
  if( maxer < 1e-8 )
    break;
	}

  // print the final error
  std::cout << iter <<" "<<maxer << std::endl;

  //--- take the following for granted for now ----
  // Write output in VTK format
  std::ofstream outf;
  outf.open("output_2dserial.vtk");
  outf << "# vtk DataFile Version 1.0" << std::endl;
  outf << "Solution of 2D heat equation" << std::endl;
  outf << "ASCII" << std::endl<< std::endl;

  outf << "DATASET RECTILINEAR_GRID" << std::endl;
  outf << "DIMENSIONS " << xnum << " " << ynum << " 1"<<std::endl<<std::endl;

  outf << "X_COORDINATES " << xnum <<" float"<<std::endl;
  for( int ii=0; ii < xnum; ii++ )
    outf << xx[ii] <<std::endl;

  outf << "Y_COORDINATES " << ynum <<" float"<<std::endl;
  for( int ii=0; ii < ynum; ii++ )
    outf << yy[ii] <<std::endl;

  outf << "Z_COORDINATES 1 float"<<std::endl;
  outf << "0" <<std::endl;

  outf << "POINT_DATA " << xnum * ynum <<std::endl;
  outf << "SCALARS temp float" <<std::endl;
  outf << "LOOKUP_TABLE default" <<std::endl;

  for( int jj=0; jj<ynum; jj++ )
  {
    for( int ii=0; ii<xnum; ii++ )
      outf << uu[ii][jj] << std::endl;
  }

	return 0;
}
