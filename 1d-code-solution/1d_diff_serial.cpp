
// TODO list
// 1. make a mistake in line 42 -- do the loop range in 0:N instead of 1:N-1
// 2. Instead of defining number of points via a macro
// use dynamic memory allocation
// 3. deallocate memory
// 4. write output in a file and compare with exact solution
// 5. write iteration count and norm in a file, and plot convergence

#include<iostream>
#include <fstream>
#include <cmath> // value of PI is defined as M_PI
#define N 16 // this is not a good practice -- use dynamic memory allocation

int main()
{
  double dt = 0.001;
  double xx[N], uold[N], unew[N];
  double g[N], f[N], rhs[N];
  double dx = 1.0/(N-1.0);
  double dxinv2 = 1.0/dx/dx;

  for( int ii=0; ii<N; ii++ )
  {
    xx[ii] = ii*dx;
    uold[ii] = 0.0;
    unew[ii] = 0.0;

    g[ii] = M_PI*M_PI*sin(M_PI*xx[ii]);
  }

  // write convergence history in a outfile
  std::ofstream outconv;
  outconv.open("convergence.txt");

  double norm  = 100.0;
  bool converged = false;
  int itercount = 0;
  while( not converged )
  {
    itercount++;
    for( int ii=1; ii<N-1; ii++ )
    {
      f[ii] = g[ii] + dxinv2 * ( uold[ii+1]-2.0*uold[ii]+uold[ii-1] );
      unew[ii] = uold[ii] + dt * f[ii];
    }

    norm  = 0.0;
    for( int ii=0; ii<N; ii++ )
    {
      if( abs(f[ii]) > norm )
        norm = abs(f[ii]);
    }

    // std::cout <<itercount<<"\t"<<norm<<std::endl;

    if( norm < 1e-6 )
    {
      std::cout<<"I am here"<<std::endl;
      converged = true;
    }

    for( int ii=0; ii<N; ii++ )
    {
      uold[ii] = unew[ii];
    }

    outconv << itercount << " " << norm << std::endl;
  }

  outconv.close(); // close the file that write convergence history

  // Write output of result in a file
  std::ofstream outfile;
  double uexact;
  outfile.open("output.txt");
  for( int ii=0; ii<N; ii++ )
  {
    uexact = sin(M_PI*xx[ii]);
    outfile << xx[ii] << "\t" << unew[ii] << "\t" << uexact << std::endl;
  }
  outfile.close();
}
