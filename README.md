# Overview

This repository contains training material of the following workshop conducted by IIT Goa nodal centre within the National Supercomputing Mission (NSM) framework.

** MPI in action: parallellization of unsteady heat conduction solvers **

It contains the following folders

1. slides
2. basicMPI -- some simple programs to review basic MPI functions
3. 1d-code-exercises -- Serial and parallel codes for solving 1D heat conduction equation [incomplete]. Used as exercises for the participants.
4. 1d-code-solution -- Completed 1D codes
5. 2d-code-exercises -- Serial and parallel codes for 2D equation [incomplete]
5. 2d-code-solution -- Completed code for unsteady problem
